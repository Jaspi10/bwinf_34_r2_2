

import java.util.ArrayList;
import java.util.List;

public class Schritt {

	//Anweisungen zur verschiebung der Pakete
	public Richtung[][] paketVerschiebungen;
	
	//Pfade aus denen der Schritt konstruiert wurde
	private List<Pfad> pfade = new ArrayList<Pfad>();

	//Stadt, in der der Schritt gueltig ist
	public final Stadt stadt;
	
	public Schritt(Stadt s){
		this.stadt = s;
		
		this.paketVerschiebungen = new Richtung[stadt.groesseX][stadt.groesseY];
		
		//Initialisiere paketVerschiebungen mit KEINE
		for(int x = 0; x < stadt.groesseX; x++){
			for(int y = 0; y < stadt.groesseY; y++){
				this.paketVerschiebungen[x][y] = Richtung.KEINE;
			}
		}
	}
	
	//Gebe den Schritt mit Unicode Pfeilen aus
	public void printSchritt() {
		for (int y = 0; y < stadt.groesseX; y++) {
			for (int x = 0; x < stadt.groesseY; x++) {
				System.out.print(paketVerschiebungen[x][y].getPrintChar());
			}
			System.out.print("\n");
		}
	}
	
	//F�gt den Pfad hinzu, wenn er sich nicht mit einem anderen �berschneidet
	public void add(Pfad p){
		
		//Pr�fe �berdeckung
		for(Pfad vorhanden : this.pfade){
			if(vorhanden.ueberdeckt(p)){
				return;
			}
		}
		
		int x = p.startX;
		int y = p.startY;
		
		//Wandere den Pfad entlang und setze die Richtung
		for(Richtung r : p.pfad){
			paketVerschiebungen[x][y] = r;
			
			x += r.getDX();
			y += r.getDY();
		}
		
		//F�ge den Pfad zur Liste hinzu um �berschneidungen zu verhindern
		pfade.add(p);
	}
	
	

}
