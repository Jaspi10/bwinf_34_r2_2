

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

public class Pfad {
	
	//Startposition
	int startX;
	int startY;
	
	//Richtungen, in die die Pakte verschoben werden
	List<Richtung> pfad = new ArrayList<Richtung>();
	
	//Stadt, in der der Pfad g�ltig ist
	Stadt stadt;
	
	//Konstruktor
	public Pfad(Stadt s, int startX, int startY){
		this.stadt = s;
		this.startX = startX;
		this.startY = startY;
	}

	//Generiert einen Pfad zuf�llig, der in der Satdt gueltig ist
	public static Pfad generiereZufaelligenPfad(Stadt s){
		Random r = new Random();
		
		//R�ckgabewert
		Pfad rv = new Pfad(s, r.nextInt(s.groesseX), r.nextInt(s.groesseY));
		
		//Aktuelle Position in der Stadt
		int aktuellX = rv.startX;
		int aktuellY = rv.startY;
		
		//Entfernung vom Start
		int dX = 0;
		int dY = 0;
		
		boolean ausweitung = true;
		
		//Koordinaten,die der Pfad beinhaltet
		Set<Koordinate> vorhanden = new TreeSet<Koordinate>();
		
		//Mindestens eine Rcihtung
		do{
			
			//Liste mit allen m�glichen Richtungen
			List<Richtung> auswahl = new ArrayList<Richtung>();

			//Darf noch erweitert werden?
			if(ausweitung){
				
				for(Richtung richtung : Richtung.values()){
					//Koordinate wenn in die Richtung gegangen werden w�rde 
					Koordinate k = new Koordinate(aktuellX + richtung.getDX(), aktuellY + richtung.getDY());
					
					//Ist die Richtung erlaubt (innerhalb der Stadt und schneidet nicht den Pfad)
					if(richtung != Richtung.KEINE && !vorhanden.contains(k) && k.istInnerhalb(0, 0, s.groesseX, s.groesseY)){
						auswahl.add(richtung);
					}
				}
			}else{
				Koordinate k;
				
				//Pr�fe f�r alle Rcihtungen die Bedingungen von oben und ob es n�her zum start geht
				
				//Westen
				k = new Koordinate(aktuellX + Richtung.WESTEN.getDX(), aktuellY + Richtung.WESTEN.getDY());
				if(dX > 0  && !vorhanden.contains(k) && k.istInnerhalb(0, 0, s.groesseX, s.groesseY)){
					auswahl.add(Richtung.WESTEN);
				}
				
				//Osten
				k = new Koordinate(aktuellX + Richtung.OSTEN.getDX(), aktuellY + Richtung.OSTEN.getDY());
				if(dX < 0 && !vorhanden.contains(k) && k.istInnerhalb(0, 0, s.groesseX, s.groesseY)){
					auswahl.add(Richtung.OSTEN);
				}
				
				//Sueden
				k = new Koordinate(aktuellX + Richtung.SUEDEN.getDX(), aktuellY + Richtung.SUEDEN.getDY());
				if(dY < 0 && !vorhanden.contains(k) && k.istInnerhalb(0, 0, s.groesseX, s.groesseY)){
					auswahl.add(Richtung.SUEDEN);
				}
				
				//Norden
				k = new Koordinate(aktuellX + Richtung.NORDEN.getDX(), aktuellY + Richtung.NORDEN.getDY());
				if(dY > 0 && !vorhanden.contains(k) && k.istInnerhalb(0, 0, s.groesseX, s.groesseY)){
					auswahl.add(Richtung.NORDEN);
				}
			}
			
			//Sackgasse
			if(auswahl.isEmpty()) return generiereZufaelligenPfad(s);
			
			//N�chste Richtung bestimmen
			Richtung neueRichtung = auswahl.get(r.nextInt(auswahl.size()));
			
			//Richtung zum Pfad hinzuf�gen
			rv.pfad.add(neueRichtung);
			
			//F�ge Koordinate ob 2. Richtung hinzu
			if(rv.pfad.size() > 1) vorhanden.add(new Koordinate(aktuellX, aktuellY));
			
			//Aktualisere die Position
			aktuellX += neueRichtung.getDX();
			aktuellY += neueRichtung.getDY();
			dX += neueRichtung.getDX();
			dY += neueRichtung.getDY();
			
			//�ndere Modus
			if(r.nextDouble() < 0.2) ausweitung = false;
			
		//Solange er nicht wieder am Anfang angekommen ist
		}while(!(dX == 0 && dY == 0));

		return rv;
	}
	
	//Generiert anzahl Pfade, die in stadt passeb und gebe sie zur�ck, wenn sie besser als mindestbewertung sind
	public static List<Pfad> generiereViele(Stadt stadt, int anzahl, double mindestBewertung){
		System.out.println("Versuche " + anzahl + " mal einen Pafd mit einer Bewertung von mindestens " + mindestBewertung + " zu genrieren.");
		
		//R�ckgabewert
		List<Pfad> rv = new ArrayList<Pfad>();
		
		for(int i = 0; i < anzahl; i++){
			Pfad p = generiereZufaelligenPfad(stadt);
			
			if(p.bewerte() > mindestBewertung){
				rv.add(p);
			}
		}
		
		return rv;
	}
	
	public double bewerte(){
		
		//R�ckgabewert
		double bewertung = 0;
		
		//Fange bei der Startposition an
		int x = this.startX;
		int y = this.startY;
		
		//Wandere den Pfad entlang
		for(int i = 0; i < this.pfad.size(); i++){
			int dX = stadt.pakete[x][y].zielX - x;
			int dY = stadt.pakete[x][y].zielY - y;
			
			Richtung r = this.pfad.get(i);
			
			//Weise dem Paket einen Wert zu
			double wert = 1 + ((double) stadt.getDistanz(x, y) / stadt.getMaxDistanz());
			
			//Pr�fe ob eine verschiebung in Richtung r gut oder schlecht ist 
			//und passe bewertung dementsprechend an
			switch (r) {
			case NORDEN:
				if(dY < 0){
					bewertung += wert;
				}else{
					bewertung -= wert;
				}
				break;
				
			case OSTEN:
				if(dX > 0){
					bewertung += wert;
				}else{
					bewertung -= wert;
				}
				break;
			
			case SUEDEN:
				if(dY > 0){
					bewertung += wert;
				}else{
					bewertung -= wert;
				}
				break;
				
			case WESTEN:
				if(dX < 0){
					bewertung += wert;
				}else{
					bewertung -= wert;
				}
				break;
			}
			
			//Bewege dich zum n�chsten Feld
			x += r.getDX();
			y += r.getDY();
			
		}
		
		//Passe den Wert so an, dass er von der L�nge unabh�ngig ist
		return bewertung / pfad.size();
	}
	
	//Liefert alle Koordinaten, die dieser Pfad betrifft
	public List<Koordinate> getKoordinaten(){
		//R�ckgabewert
		List<Koordinate> rv = new ArrayList<Koordinate>();
		
		int x = this.startX;
		int y = this.startY;
		
		//Wandere den Pfad entlang
		for(Richtung r : this.pfad){
			x += r.getDX();
			y += r.getDY();
			
			rv.add(new Koordinate(x, y));
		}
		
		return rv;
	}
	
	//Pr�fe ob irgendeine der Koordinaten von p auch eine Koordinate von uns ist
	public boolean ueberdeckt(Pfad p){
		List<Koordinate> koordinatenP = p.getKoordinaten();
		
		//Haben wir Koordinaten von p?
		for(Koordinate k : this.getKoordinaten()){
			if(koordinatenP.contains(k)){
				return true;
			}
		}
		
		return false;
	}
}
