

public class Paket {
	
	//X Position des Ziels des Pakets
	public final int zielX;
	
	//Y Position des Ziels des Pakets
	public final int zielY;
	
	//Konstruktor
	public Paket(int zielX, int zielY) {
		this.zielX = zielX;
		this.zielY = zielY;
	}
}
