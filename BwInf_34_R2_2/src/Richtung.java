

public enum Richtung {
	//Himmelsrichtungen und Keine
	NORDEN(0, -1), OSTEN(1, 0), SUEDEN(0, 1), WESTEN(-1, 0), KEINE(0, 0);

	//Differenz zum Haus auf das diese Richtung zeigt
	private final int dX, dY;

	//Konstruktor
	private Richtung(int dX, int dY) {
		this.dX = dX;
		this.dY = dY;
	}

	//Zeichen zur Ausgabe in die Datei
	public char getChar() {
		switch (this) {
		case NORDEN:
			return 'N';
		case OSTEN:
			return 'O';
		case WESTEN:
			return 'W';
		case SUEDEN:
			return 'S';
		case KEINE:
			return '_';
		default:
			return 'E';
		}
	}

	//Zeichen zur Ausgabe in die Konsole (Unicode Pfeile)
	public char getPrintChar() {
		switch (this) {
		case NORDEN:
			return '\u2191';
		case OSTEN:
			return '\u2192';
		case WESTEN:
			return '\u2190';
		case SUEDEN:
			return '\u2193';
		case KEINE:
			return '\u25cb';
		default:
			return 'E';
		}
	}

	public int getDX() {
		return dX;
	}

	public int getDY() {
		return dY;
	}
}
