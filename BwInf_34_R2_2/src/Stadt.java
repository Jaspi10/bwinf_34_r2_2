
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Stadt {

	//Pakete dieser Stadt
	public final Paket[][] pakete;
	
	//Groesse der Stadt
	public final int groesseX;
	public final int groesseY;
	
	//Zur zwischenspeicherung von maxDistanz
	private int maxDistanz = -1;

	//Konstruktor 
	private Stadt(int groesseX, int groesseY, Paket[][] pakete) {
		this.groesseX = groesseX;
		this.groesseY = groesseY;
		this.pakete = pakete;
	}

	//Liest die eine Stadt aus einer Eingabedatei
	public static Stadt leseAusDatei(File f) {
		System.out.println("Lese Stadt aus " + f.getAbsolutePath() + ".");
		
		try {
			BufferedReader r = new BufferedReader(new FileReader(f));

			//Gr��e lesen
			int size = Integer.parseInt(r.readLine());

			//Gr��e x Gr��e Array initialisieren
			Paket[][] p = new Paket[size][size];

			//F�r alle Zeilen
			for (int i = 0; i < size * size; i++) {
				
				String raw = r.readLine();
				
				//Zeile aufteilen
				String[] raw_split = raw.split(" ");
				
				//Teile in int werte umwandeln
				int[] split = new int[4];

				for (int j = 0; j < raw_split.length; j++) {
					split[j] = Integer.parseInt(raw_split[j]);
				}
				
				//Pakete in das Array eintragen
				//WICHTIG: Koordinaten werden vertauscht
				p[split[1]][split[0]] = new Paket(split[3], split[2]);
			}
			
			r.close();
			
			return new Stadt(size, size, p);
		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
		}
		return null;

	}
	
	//Liefert die Distanz zwischen dem Paket an (x|y) und seinem Ziel
	public int getDistanz(int x, int y){
		return Math.abs(x - pakete[x][y].zielX) + Math.abs(y - pakete[x][y].zielY);
	}
	
	//Liefert die gr��te Distanz, zwischen einem Paket und seinem Ziel
	public int getMaxDistanz(){
		//Wurde maxDistanz schonmal berechnet?
		if(maxDistanz != -1) return maxDistanz;
		
		//R�ckgabewert
		int rv = 0;
		
		//Maximale Distanz berechnen
		for(int x = 0; x < groesseX; x++){
			for(int y = 0; y < groesseY; y++){
				rv = Math.max(rv, getDistanz(x, y));
			}
		}
		
		//Wert speichern
		maxDistanz = rv;
		
		return rv;
	}

	//Erstellt eine Stadt, in der die Pakete nach den Informationen aus Schritt verschoben werden
	public Stadt macheSchritt(Schritt schritt) {
		//R�ckgabewert
		Paket[][] rv = new Paket[groesseX][groesseY];

		//F�r alle Pakete
		for (int i = 0; i < groesseX; i++) {
			for (int j = 0; j < groesseY; j++) {
				int dX = schritt.paketVerschiebungen[i][j].getDX();
				int dY = schritt.paketVerschiebungen[i][j].getDY();

				//Verschiebe das Paket
				rv[i + dX][j + dY] = pakete[i][j];
			}
		}

		return new Stadt(groesseX, groesseY, rv);
	}
	
	//GUI
	public Paket getPaket(int x, int y){
		return pakete[x][y];
	}
}
