

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Loesung {

	//Urspr�ngliche stadt
	Stadt ausgangsStadt;
	
	//Schritte die zur Loesung f�hren
	List<Schritt> schritte = new ArrayList<Schritt>();

	//Konstruktor (Privat)
	private Loesung(Stadt ausgangsStadt) {
		this.ausgangsStadt = ausgangsStadt;
	}

	//Schreibt die Loesung in dem auf der Website angegebenen Format in die Datei
	public void schreibeLoesung(File f) {
		try {
			BufferedWriter w = new BufferedWriter(new FileWriter(f));

			//F�r Alle Pakte
			for (int x = 0; x < ausgangsStadt.groesseX; x++) {
				for (int y = 0; y < ausgangsStadt.groesseY; y++) {
					//Schreibe Koordinaten 
					w.write(y + " " + x + " ");

					//Schreibe Richtungen
					for (Schritt s : schritte) {
						w.write(s.paketVerschiebungen[x][y].getChar());
					}

					//Neue Zeile
					w.write("\n");
				}
			}

			w.flush();
			w.close();
		} catch (IOException e) {

		}

	}

	//Generiert eine gueltige Folge von Schritten, die alle Pakete an ihr Ziel bringt
	public static Loesung generiereLoesung(Stadt stadt) {
		//R�ckgabewert
		Loesung rv = new Loesung(stadt);

		//Solange es noch ein Paket gibt, dass nicht an seinem Zeil ist
		while (stadt.getMaxDistanz() > 0) {

			System.out.println();
			System.out.println("Gr��te vorhandene Entfernung zwischen Paket und Ziel ist " + stadt.getMaxDistanz());
			
			//Versuche eine Million mal einen Pfad zu generieren und gebe alle zur�ck die mindestens eine Bewertung von -0.6 haben
			List<Pfad> moeglichePfade = Pfad.generiereViele(stadt, 1000000, -0.6);

			//Sortiere die Pfade nach Bewertung und bei gleicher Bewertung nach l�nge
			System.out.println("Sortiere Pfade");
			moeglichePfade.sort((Pfad p1, Pfad p2) -> p2.pfad.size() - p1.pfad.size());
			moeglichePfade.sort((Pfad p1, Pfad p2) -> Double.compare(p2.bewerte(), p1.bewerte()));

			Schritt aktuellerSchritt = new Schritt(stadt);

			//Baue den Schritt
			System.out.println("Konstruiere Schritt");
			for (Pfad p : moeglichePfade) {
				aktuellerSchritt.add(p);
			}

			//F�hre den Schritt durch und f�ge ihn zur Liste hinzu
			stadt = stadt.macheSchritt(aktuellerSchritt);
			rv.schritte.add(aktuellerSchritt);
		}

		return rv;

	}

}
