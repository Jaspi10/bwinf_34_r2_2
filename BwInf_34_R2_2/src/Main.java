

import java.io.File;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		
			// Initialisiere Stadt
			Stadt s = Stadt.leseAusDatei(new File(args[0]));

			// Berechne Loesung
			Loesung l = Loesung.generiereLoesung(s);

			// TODO: AUSGABE
			System.out.println(l.schritte.size());

			for (int i = 0; i < l.schritte.size(); i++) {
				System.out.println(i + "-ter Schritt:");
				l.schritte.get(i).printSchritt();
				System.out.println("\n");
			}
			
			if(args.length >= 2){
				l.schreibeLoesung(new File(args[1]));
			}else{
				l.schreibeLoesung(new File(args[0] + ".loesung"));
			}

		}
	}

