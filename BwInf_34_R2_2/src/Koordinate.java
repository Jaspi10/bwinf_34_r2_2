

public class Koordinate implements Comparable<Koordinate>{
	
	//Größter wert den x annehmen sollte, damit hashcode und compareTo korrekt funktionieren
	private static int maxX = (int) Math.sqrt(Integer.MAX_VALUE);

	//Position
	public final int x;
	public final int y;
	
	//Konstruktor
	public Koordinate(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	//Überschriebene Gleichheitsmethode
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Koordinate)){
			return false;
		}else{
			Koordinate k = (Koordinate) obj;
			return this.x == k.x && this.y == k.y;
		}
	}

	//Vergleicht die hashcodes
	@Override
	public int compareTo(Koordinate k) {
		return this.hashCode() - k.hashCode();
	}
	
	//Ordnet jeder Koordinate mit x < sqrt(Integer.MAX_VALUE) 
	//und y < sqrt(Integer.MAX_VALUE) einen Eindeutigen Wert zu
	@Override
	public int hashCode() {
		return (this.x + this.y * maxX);
	}
	
	public boolean istInnerhalb(int minX, int minY, int maxX, int maxY){
		return (x >= minX) && (y >= minY) && (x < maxX) && (y < maxY);
	}
}
